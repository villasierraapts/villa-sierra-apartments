Villa Sierra Apartments offers 1, 2 & 3 bedroom apartments for rent in El Paso, TX near Fort Bliss and UTEP. 785 to 1,570 square feet. El Paso apartments have fireplace, gas stove, & walk-in closet. Select units w/updated kitchen & washer/dryer hookups. Amenities include theater, dog park & pools.

Address: 2435 McKinley Ave, El Paso, TX 79930, USA

Phone: 915-562-4119